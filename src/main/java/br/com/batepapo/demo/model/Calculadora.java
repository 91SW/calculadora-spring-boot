/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.batepapo.demo.model;

import java.io.Serializable;
import java.math.BigDecimal;
import lombok.Data;

/**
 * Objeto para manter os números utilizados no cálculo solicitado.
 * O @Data é uma anotação do lombok que abstrai a criação de getter, setter e 
 * construtores. Deve ser usado com cautela, principalmente, quando devemos
 * garantir o uso correto do encapsulamento.
 */
@Data
public class Calculadora implements Serializable {

    private BigDecimal numeroPrincipal;
    private BigDecimal numeroSecundario;
}
