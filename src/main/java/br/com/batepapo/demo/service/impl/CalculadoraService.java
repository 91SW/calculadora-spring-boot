/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.batepapo.demo.service.impl;

import br.com.batepapo.demo.model.Calculadora;
import br.com.batepapo.demo.service.CalculadoraInterface;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.Objects;
import org.springframework.stereotype.Service;

/**
 * A classe service é responsável por receber os objetos/dados que entram pelo
 * controller e transferir as responsabilidades para outras camadas da
 * arquitetura. Neste exemplo, estamos aplicando as regras de negócio no service
 * pela baixa complexidade da arqutetura e lógica envolvida.
 */
@Service
public class CalculadoraService implements CalculadoraInterface {

    @Override
    public BigDecimal calculoAdicao(Calculadora calculadora) {
        BigDecimal numeroPrimario = calculadora.getNumeroPrincipal();
        BigDecimal numeroSecundario = calculadora.getNumeroSecundario();
        
        return numeroPrimario.add(numeroSecundario);
    }

    @Override
    public BigDecimal calculoSubtracao(Calculadora calculadora) {
        BigDecimal resultado = calculadora.getNumeroPrincipal();

        return resultado.subtract(calculadora.getNumeroSecundario());
    }

    @Override
    public BigDecimal calculoMultiplicacao(Calculadora calculadora) {
        BigDecimal resultado = calculadora.getNumeroPrincipal();

        return resultado.multiply(calculadora.getNumeroSecundario());
    }

    @Override
    public BigDecimal calculoDivisao(Calculadora calculadora) {

        if (Objects.equals(calculadora.getNumeroSecundario(), 
                new BigDecimal(BigInteger.ZERO))) {
            throw new IllegalArgumentException("Erro! Divisão por zero não pode ser aceita.");
        }

        BigDecimal resultado = calculadora.getNumeroPrincipal();

        return resultado.divide(calculadora.getNumeroSecundario(), new MathContext(2, RoundingMode.HALF_UP));
    }

}
