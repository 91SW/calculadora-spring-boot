/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.batepapo.demo.service;

import br.com.batepapo.demo.model.Calculadora;
import java.math.BigDecimal;

/**
 * Interface que conterá a assinatura dos métodos usados no service.
 *
 */
public interface CalculadoraInterface {

    BigDecimal calculoAdicao(Calculadora calculadora);

    BigDecimal calculoSubtracao(Calculadora calculadora);

    BigDecimal calculoMultiplicacao(Calculadora calculadora);

    BigDecimal calculoDivisao(Calculadora calculadora);

}
