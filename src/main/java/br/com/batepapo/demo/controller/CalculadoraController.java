/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.batepapo.demo.controller;

import br.com.batepapo.demo.model.Calculadora;
import br.com.batepapo.demo.service.impl.CalculadoraService;
import java.math.BigDecimal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author admin
 */
@RestController
@RequestMapping(value = "calculadora/")
public class CalculadoraController {

    @Autowired
    CalculadoraService service;

    @GetMapping(value = "/soma/{numeroPrimario}/{numeroSecundario}")
    public BigDecimal calculoAdicao(@PathVariable(value = "numeroPrimario") BigDecimal numeroPrimario,
            @PathVariable(value = "numeroSecundario") BigDecimal numeroSecundario) {
        Calculadora calculadora = new Calculadora();
        calculadora.setNumeroPrincipal(numeroPrimario);
        calculadora.setNumeroSecundario(numeroSecundario);

        return service.calculoAdicao(calculadora);
    }

    @GetMapping(value = "/subtracao/{numeroPrimario}/{numeroSecundario}")
    public BigDecimal calculoSubtracao(@PathVariable(value = "numeroPrimario") BigDecimal numeroPrimario,
            @PathVariable(value = "numeroSecundario") BigDecimal numeroSecundario) {
        Calculadora calculadora = new Calculadora();
        calculadora.setNumeroPrincipal(numeroPrimario);
        calculadora.setNumeroSecundario(numeroSecundario);

        return service.calculoSubtracao(calculadora);
    }

    @GetMapping(value = "/multiplicacao/{numeroPrimario}/{numeroSecundario}")
    public BigDecimal calculoMultiplicacao(@PathVariable(value = "numeroPrimario") BigDecimal numeroPrimario,
            @PathVariable(value = "numeroSecundario") BigDecimal numeroSecundario) {
        Calculadora calculadora = new Calculadora();
        calculadora.setNumeroPrincipal(numeroPrimario);
        calculadora.setNumeroSecundario(numeroSecundario);

        return service.calculoMultiplicacao(calculadora);
    }

    @GetMapping(value = "/divisao/{numeroPrimario}/{numeroSecundario}")
    public BigDecimal calculoDivisao(@PathVariable(value = "numeroPrimario") BigDecimal numeroPrimario,
            @PathVariable(value = "numeroSecundario") BigDecimal numeroSecundario) {
        Calculadora calculadora = new Calculadora();
        calculadora.setNumeroPrincipal(numeroPrimario);
        calculadora.setNumeroSecundario(numeroSecundario);

        return service.calculoDivisao(calculadora);
    }

    @PostMapping(value = "/soma")
    public BigDecimal calculoAdicao(@RequestBody Calculadora calculadora) {
        return service.calculoAdicao(calculadora);
    }
}
